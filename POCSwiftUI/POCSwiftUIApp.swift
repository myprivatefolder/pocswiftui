//
//  POCSwiftUIApp.swift
//  POCSwiftUI
//
//  Created by Sudeb on 19/05/22.
//

import SwiftUI

@main
struct POCSwiftUIApp: App {
    
    @StateObject var viewRouter = ViewRouter()

    var body: some Scene {
        WindowGroup {
            ContentView(viewRouter: viewRouter)
        }
    }
}
