//
//  View+Extensions.swift
//  MoviesApp
//
//  Created by Mohammad Azam on 7/31/20.
//  Copyright © 2020 Mohammad Azam. All rights reserved.
//

import Foundation
import SwiftUI

extension View {
    
    func embedNavigationView() -> some View {
        let naviGationView = NavigationView {
            
        }.listStyle(.plain)
            .navigationTitle("Search NearBy")
        return NavigationView {self}
    }
    
}
