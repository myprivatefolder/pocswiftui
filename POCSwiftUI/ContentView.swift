//
//  ContentView.swift
//  POCSwiftUI
//
//  Created by Sudeb on 19/05/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewRouter: ViewRouter

    var body: some View {
        
        TabBarView(viewRouter: viewRouter)

    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewRouter: ViewRouter())
    }
}
