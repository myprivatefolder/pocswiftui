//
//  FavouriteView.swift
//  POCSwiftUI
//
//  Created by Sudeb on 20/05/22.
//

import SwiftUI

struct FavouriteView: View {
    var body: some View {
        Text("Fav, World!")
    }
}

struct FavouriteView_Previews: PreviewProvider {
    static var previews: some View {
        FavouriteView()
    }
}
