//
//  DetailsView.swift
//  POCSwiftUI
//
//  Created by Sudeb on 20/05/22.
//

import SwiftUI

import MapKit


struct DetailsView: View {
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
    
    var body: some View {
        NavigationView {
            ZStack {
                Map(coordinateRegion: $region)
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(spacing: 20) {
                        ForEach(0..<10) {_ in
                            DetailsCell()
                        }
                    }.padding(.leading , 30)
                }.frame(width: UIScreen.main.bounds.width, height: 150, alignment: .center)
                    .padding(.top, 400)
                
            }
        }
       
        
    }
}

struct DetailsView_Previews: PreviewProvider {
    static var previews: some View {
        DetailsView()
    }
}


struct DetailsCell : View {
    
    var body: some View {
        HStack {
            //Color.gray.ignoresSafeArea()
            
            Image("hotels")
                .resizable()
                //.scaledToFill()
               // .aspectRatio(1 / 1, contentMode: .fill)
                .cornerRadius(2)
                .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.orange, lineWidth: 4))
                .padding(.bottom,40)
                .padding(.leading,10)
                .shadow(radius: 10)
                .frame(width: 100, height: 120, alignment: .leading)
            
            VStack {
                Text("Title text").font(.title).padding(.top,10)
                Text("Sub Title ajkhaehkq")
                   // .padding(.leading, 20).font(.body)
            }.padding(.top , -60)
           
            Spacer()
            
        }.frame(width: (UIScreen.main.bounds.width - 50), height: 150, alignment: .bottom)
            .background(Color("SearchBar").clipShape(RoundedRectangle(cornerRadius:5)))
            .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.gray, lineWidth: 2))
        
    }
}
 
