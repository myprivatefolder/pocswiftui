//
//  NearByView.swift
//  POCSwiftUI
//
//  Created by Sudeb on 20/05/22.
//

import SwiftUI
import MapKit

struct NearByView: View {
    
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
    var arrMapOption  = ["Hotels","Resturants","Doctors","Gym"]
    var body: some View {
        NavigationView {
            ZStack {
                
                Map(coordinateRegion: $region)
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(spacing: 20) {
                        ForEach(0..<arrMapOption.count) {
                            MapFilterOption(title: arrMapOption[$0])
                        }
                    }.padding(.leading , 30)
                }.frame(width: UIScreen.main.bounds.width, height: 50, alignment: .center)
                    .padding(.top,(600 - UIScreen.main.bounds.height ))
                    //.background(.red)
            }
        }
    }
}

struct MapFilterOption : View {
    @State var title = ""
    var body: some View {
        HStack {
            //Color.gray.ignoresSafeArea()
            VStack {
                Text(title)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding(10)
                   // .padding(.leading, 20).font(.body)
            }
            Spacer()
            
        }
            .background(Color("SearchBar").clipShape(RoundedRectangle(cornerRadius:25)))
            .overlay(RoundedRectangle(cornerRadius: 25)
                        .stroke(Color.gray, lineWidth: 2))
        
    }
}


struct NearByView_Previews: PreviewProvider {
    static var previews: some View {
        NearByView()
    }
}
