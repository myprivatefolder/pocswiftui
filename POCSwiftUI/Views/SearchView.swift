//
//  SearchView.swift
//  POCSwiftUI
//
//  Created by Sudeb on 20/05/22.
//

import SwiftUI

struct SearchView: View {
    
    @Binding var searchText: String
    @Binding var searching: Bool
    
    var body: some View {
        VStack {
            ZStack {
                Rectangle()
                    .foregroundColor(Color("SearchBar"))
                HStack {
                    Image(systemName: "magnifyingglass")
                    TextField("Search ...", text: $searchText) { startedSearching in
                        if startedSearching {
                            withAnimation {
                                searching = true
                            }
                        }
                    } onCommit: {
                        withAnimation {
                            searching = false
                        }
                    }
                }
                .foregroundColor(.gray)
                .padding()
            }
        }
        .frame(height: 40)
        .cornerRadius(13)
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(searchText: .constant(""), searching: .constant(false))
    }
}
