//
//  ViewRouter.swift
//  POCSwiftUI
//
//  Created by Sudeb on 19/05/22.
//
import SwiftUI

class ViewRouter: ObservableObject {
    
    @Published var currentPage: Page = .home
    
}


enum Page {
    case home
    case liked
}
