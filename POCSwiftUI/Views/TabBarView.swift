//
//  TabBarView.swift
//  POCSwiftUI
//
//  Created by Sudeb on 19/05/22.
//

import SwiftUI

struct TabBarView : View {
    @StateObject var viewRouter: ViewRouter
    
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                VStack {
                    Spacer()
                    switch viewRouter.currentPage {
                    case .home:
                        VStack {
                            SearchView(searchText: .constant(""), searching: .constant(false))
                            NearByView()
                        }
                        .navigationBarTitle("Nearby",displayMode: .inline)
                        // Home Navigation
                        //Text("Home")
                    case .liked:
                        // Like Navigation
                        DetailsView()
                    }
                    Spacer()
                    ZStack {
                        HStack {
                            TabBarIcon(viewRouter: viewRouter, assignedPage: .home, width: geometry.size.width/2, height: geometry.size.height/28, systemIconName: "homekit", tabName: "Home")
                            TabBarIcon(viewRouter: viewRouter, assignedPage: .liked, width: geometry.size.width/2, height: geometry.size.height/28, systemIconName: "heart", tabName: "Liked")
                            
                        }
                        .frame(width: geometry.size.width, height: geometry.size.height/8)
                        .background(Color("TabBarBackground").shadow(radius: 2))
                        .navigationBarTitle("Favourite",displayMode: .inline)
                    }
                }.edgesIgnoringSafeArea(.bottom)
            }
        }
    }
}

struct TabBarIcon: View {
    @StateObject var viewRouter: ViewRouter
    let assignedPage: Page
    
    let width, height: CGFloat
    let systemIconName, tabName: String
    
    var body: some View {
        VStack {
            Image(systemName: systemIconName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: width, height: height)
                .padding(.top, 10)
            Text(tabName)
                .font(.footnote)
            Spacer()
        }
        .padding(.horizontal, -4)
        .onTapGesture {
            viewRouter.currentPage = assignedPage
        }
        .foregroundColor(viewRouter.currentPage == assignedPage ? Color("TabBarHighlight") : .gray)
    }
}


